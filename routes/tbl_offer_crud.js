var express = require("express")
var router = express.Router()
var sequelize = require('./connection/connection')
var async = require('async')
var tbl_offer = sequelize.import('./models/tbl_offer')
var tbl_order = sequelize.import('./models/tbl_order')
var tbl_order_item = sequelize.import('./models/tbl_order_item')
var tbl_cart = sequelize.import('./models/tbl_cart')
var tbl_price = sequelize.import('./models/tbl_price')
var Op = sequelize.Op
var moment = require('moment')

router.post("/add",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.name=="" || data.name==null){
                    callback("Offer name can not be null")
                }else{
                    if(data.code=="" || data.code==null){
                        callback("Offer code can not null")
                    }else{
                        if(data.description=="" || data.description==null){
                            callback("Description can not be null")
                        }else{
                            if(data.discount=="" || data.discount==null || data.discount==0){
                                callback("discount can not be null or zero")
                            }else{
                                if(data.maxdiscount=="" || data.maxdiscount==null || data.maxdiscount==0){
                                    callback("maxdiscount can not be null or zero")
                                }else{
                                    if(data.mincartvalue=="" || data.mincartvalue==null || data.mincartvalue==0){
                                        callback("mincartvalue can not be null or zero")
                                    }else{
                                        if(data.startdate=="" || data.startdate==null){
                                            callback("start date can not be null")
                                        }else{
                                            if(moment(data.startdate,moment.ISO_8601,true).isValid()){
                                                if(data.enddate=="" || data.enddate==null){
                                                    callback("enddate can not be null")
                                                }else{
                                                    if(moment(data.enddate,moment.ISO_8601,true).isValid()){
                                                        if(data.enddate<=data.startdate || Date.parse(data.startdate) < Date.parse(Date()) ){
                                                            callback("Time period is not valid")
                                                        }else{
                                                            callback(null)
                                                        }
                                                    }else{
                                                        callback("enddate is not in ISO format")
                                                    }
                                                }
                                            }else{
                                                callback("startdate is not in ISO format")
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            function(callback){
                tbl_offer.create({
                    name:data.name,
                    code:data.code,
                    description:data.description,
                    startdate:data.startdate,
                    enddate:data.enddate,
                    discount:data.discount,
                    maxdiscount:data.maxdiscount,
                    mincartvalue:data.mincartvalue
                })
                .then((offer)=>{
                    if(offer!=null){
                        res.send({
                            isSuccess:1,
                            Message:"Offer added successfully",
                            Response:offer
                        })
                    }else{
                        callback("Failed to add offer")
                    }
                },(err)=>{
                    callback(err["name"])
                })
            }
        ],
            function(err,result){
                res.send({
                    isSuccess:0,
                    Message:err,
                })
            })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

router.post("/get",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.user_id=="" || data.user_id==null || data.user_id==0){
                    callback("user_id can not be null or zero")
                }else{
                    if(data.totalamount=="" || data.totalamount==null){
                        callback("totalamount can not be null")
                    }else{
                        callback(null)
                    }
                }
            },
            function(callback){
                tbl_order.findAll({
                    attributes:["offer_id"],
                    where:{
                        user_id:data.user_id,
                        offer_id:{
                            [Op.ne]:null
                        }
                    }
                })
                .then(offer=>{
                    offer = offer.map(offer=>{
                        return offer.offer_id
                    })
                    return tbl_offer.findAll({
                        where:{
                            id:{
                                [Op.notIn]:offer,
                            },
                            flag:1,
                            mincartvalue:{
                                [Op.lte]:data.totalamount
                            }
                        }
                    })
                })
                .then((offer)=>{
                    if(offer[0]!=null){
                        res.send({
                            isSuccess:1,
                            Message:"Offer found",
                            Response:offer
                        })
                    }else{
                        callback("offer not found")
                    }
                },(err)=>{
                    callback(err["message"])
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

router.post("/deactive",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.offer_id=="" || data.offer_id==null || data.offer_id==0){
                    callback("offer_id can not be null or zero")
                }else{
                    callback(null)
                }
            },
            function(callback){
                tbl_offer.update({
                    flag:0
                },{
                    where:{
                        id:data.offer_id,
                        flag:1
                    }
                })
                .then((offer)=>{
                    if(offer==1){
                        res.send({
                            isSuccess:1,
                            Message:"Offer deactivated"
                        })
                    }else{
                        callback("offer not found")
                    }
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

router.post("apply",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){

            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

router.post("apply",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.quantity==1){
                    if(data.user_id=="" || data.user_id==null || data.user_id==0){
                        callback("user_id can not be null or zero")
                    }else{
                        if(data.product_id=="" || data.product_id==null || data.product_id==0){
                            callback("product_id can not be null or zero")
                        }else{
                            if(data.offer_id=="" || data.offer_id==null || data.offer_id==0){
                                callback("offer_id can not be null or zero")
                            }else{
                                callback(null)
                            }
                        }
                    }
                }else{
                    callback("offer only applicable on one quantity")
                }
            },
            function(callback){
                tbl_offer.find({
                    where:{
                        id:data.offer_id,
                        flag:1,
                        startdate:{
                            [Op.lte]:Date.now()
                        },
                        enddate:{
                            [Op.gte]:Date.now()
                        }
                    }
                })
                .then((offer)=>{
                    if(offer!=null){
                        callback(null,offer)
                    }else{
                        callback("offer not exist or expired")
                    }
                },(err)=>{
                    callback(err)
                })
            },
            function(offer,callback){
                tbl_order.findAll({
                    include:[{
                        model:tbl_order_item,
                        where:{
                            offer_id:data.offer_id
                        }
                    }],
                    where:{
                        user_id:data.user_id,
                    }
                })
                .then((order)=>{
                    return tbl_cart.findAll({
                        where:{
                            user_id:data.user_id,
                            offer_id:data.offer_id,
                            Ischeckout:0
                        }
                    })
                    .then((cart)=>{
                        console.log(cart.length + order.length)
                        return cart.length + order.length
                    })
                })
                .then((order)=>{
                    if(order<offer.timeperuser){
                        callback(null,offer)
                    }else{
                        callback("you already got this offer")
                    }
                })
            },
            function(offer,callback){
                tbl_price.find({
                    attributes:["price"],
                    where:{
                        product_id:data.product_id,
                        flag:1
                    }
                })
                .then((price)=>{
                    if(price!=null){
                        discount = price.price * offer.discount /100
                        if(discount>offer.maxdiscount){
                            discount=offer.maxdiscount
                        }
                        offer.price=price.price - discount
                        callback(null,offer)
                    }else{
                        callback("product not found")
                    }
                },(err)=>{
                    callback(err)
                })
            },
            function(offer,callback){
                tbl_cart.update({
                    offer_id:data.offer_id,
                    price:offer.price
                },{
                    where:{
                        user_id:data.user_id,
                        product_id:data.product_id,
                        Ischeckout:0
                    }
                })
                .then((cart)=>{
                    if(cart[0]==1){
                        res.send({
                            isSuccess:1,
                            Message:"Offer appiled",
                            Response:offer
                        })
                    }else{
                        callback("product not found in cart")
                    }
                },(err)=>{
                    callback("product not found in cart")
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

module.exports = router