module.exports = function(sequelize,DataType){
    var tbl_offer = sequelize.define("tbl_offer",{
        name:{
            type:DataType.STRING
        },
        code:{
            type:DataType.STRING,
            unique:"compositekey"
        },
        description:{
            type:DataType.STRING
        },
        discount:{
            type:DataType.STRING
        },
        maxdiscount:{
            type:DataType.INTEGER
        },
        mincartvalue:{
            type:DataType.INTEGER
        },
        startdate:{
            type:DataType.DATE
        },
        enddate:{
            type:DataType.DATE
        },
        flag:{
            type:DataType.BOOLEAN,
            defaultValue:1
        },
        timeperuser:{
            type:DataType.INTEGER,
            defaultValue:1
        }
    },{
        tableName:"tbl_offer"
    })

    tbl_offer.sync()
    return tbl_offer
}