module.exports = function(sequelize,DataType){
    var tbl_price = sequelize.define("tbl_price",{
        price:{
            type:DataType.INTEGER
        },
        createdBy:{
            type:DataType.STRING,
        },
        flag:{
            type:DataType.BOOLEAN,
            defaultValue:1
        }
    },{
        tableName:"tbl_price",
        updatedAt:false
    })

    var tbl_product = sequelize.import('./tbl_product')
    tbl_product.hasOne(tbl_price,{
        foreignKey:"product_id"
    })
    //tbl_price.belongsTo(tbl_product)

    tbl_price.sync()
    return tbl_price
}