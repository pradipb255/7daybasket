module.exports = function(sequelize,DataType){
    var tbl_product = sequelize.define("tbl_product",{
        product_name:{
            type:DataType.STRING,
            unique:true
        },
        product_image:{
            type:DataType.STRING
        },
        category_id:{
            type:DataType.INTEGER
        },
        createdBy:{
            type:DataType.STRING,
        },
        updatedBy:{
            type:DataType.STRING
        }
    },{
        tableName:"tbl_product",
        paranoid:true,
        // hooks:{
        //     afterFind:(tbl_product)=>{
        //     tbl_product.product_image = "public/images/product/" + product_image
        //     return tbl_product.product_image
        //     }
        // }
    })

    var tbl_product_category = sequelize.import('./tbl_product_category')

    tbl_product.belongsTo(tbl_product_category,{
        foreignKey:"category_id"
    })
    
    tbl_product.sync()
    return tbl_product
}