module.exports = function(sequelize,DataType){
    var tbl_cart = sequelize.define("tbl_cart",{
        user_id:{
            type:DataType.INTEGER,
            unique:"compositekey"
        },
        product_id:{
            type:DataType.INTEGER,
            unique:"compositekey"
        },
        quantity:{
            type:DataType.FLOAT,
            defaultValue:1
        },
        price:{
            type:DataType.INTEGER
        },
        Ischeckout:{
            type:DataType.BOOLEAN,
            defaultValue:0
        }
    },{
        tableName:"tbl_cart"
    })

    var tbl_user = sequelize.import('./tbl_user')
    var tbl_product = sequelize.import('./tbl_product')

    tbl_cart.belongsTo(tbl_user,{
        foreignKey:"user_id"
    })
    tbl_cart.belongsTo(tbl_product,{
        foreignKey:"product_id"
    })

    tbl_cart.sync()
    return tbl_cart
}