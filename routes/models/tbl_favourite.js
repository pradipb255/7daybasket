module.exports = function(sequelize,DataType){
    var tbl_favourite = sequelize.define("tbl_favourite",{
        user_id:{
            type:DataType.INTEGER,
            unique:"compositekey"
        },
        product_id:{
            type:DataType.INTEGER,
            unique:"compositekey"
        },
        status:{
            type:DataType.BOOLEAN,
            defaultValue:1
        }
    },{
        tableName:"tbl_favourite",
        updatedAt:true
    })
    var tbl_product = sequelize.import('./tbl_product')
    var tbl_user = sequelize.import('./tbl_user')

    tbl_favourite.belongsTo(tbl_product,{foreignKey: "product_id"})
    tbl_favourite.belongsTo(tbl_user,{foreignKey: "user_id"})

    tbl_favourite.sync()
    return tbl_favourite
}