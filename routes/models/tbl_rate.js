module.exports = function(sequelize,DataType){
    var tbl_rate = sequelize.define("tbl_rate",{
        user_id:{
            type:DataType.INTEGER,
            unique:"compositekey"
        },
        product_id:{
            type:DataType.INTEGER,
            unique:"compositekey"
        },
        rate:{
            type:DataType.INTEGER
        },
        message:{
            type:DataType.STRING
        }
    },{
        tableName:"tbl_rate"
    })

    var tbl_user = sequelize.import('./tbl_user')
    var tbl_product = sequelize.import('./tbl_product')

    tbl_user.hasMany(tbl_rate,{foreignKey: "user_id"})
    tbl_product.hasMany(tbl_rate,{foreignKey: "product_id"})

    tbl_rate.sync()
    return tbl_rate
}