module.exports = function(sequelize,DataType){
    var tbl_product_category = sequelize.define("tbl_product_category",{
        category_name:{
            type:DataType.STRING,
            unique:true
        },
        category_image:{
            type:DataType.STRING,
        },
        createdBy:{
            type:DataType.STRING,
        },
        updatedBy:{
            type:DataType.STRING
        }
    },{
        tableName:"tbl_product_category",
        paranoid:true
    })
    
    tbl_product_category.sync()
    return tbl_product_category
}