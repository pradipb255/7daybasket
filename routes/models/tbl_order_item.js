module.exports = function(sequelize,DataType){
    var tbl_order_item = sequelize.define("tbl_order_item",{
        order_id:{
            type:DataType.INTEGER,
        },
        product_id:{
            type:DataType.INTEGER
        },
        quantity:{
            type:DataType.INTEGER
        },
        price:{
            type:DataType.INTEGER
        }
    },{
        tableName:"tbl_order_item"
    })

    var tbl_order = sequelize.import('./tbl_order')
    var tbl_product = sequelize.import('./tbl_product')

    // tbl_order_item.belongsTo(tbl_order,{
    //     foreignKey:"order_id"
    // })
    tbl_order.hasMany(tbl_order_item,{
        foreignKey:"order_id"
    })
    tbl_order_item.belongsTo(tbl_product,{
        foreignKey:"product_id"
    })

    tbl_order_item.sync()
    return tbl_order_item
}