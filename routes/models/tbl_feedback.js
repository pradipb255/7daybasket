module.exports = function(sequelize,DataType){
    var tbl_feedback = sequelize.define("tbl_feedback",{
        league_name:{
            type:DataType.STRING,
        },
        product_id:{
            type:DataType.INTEGER,
            unique:"compositekey"
        },
        message:{
            type:DataType.STRING
        }
    },{
        tableName:"tbl_feedback"
    })

    var tbl_user = sequelize.import('./tbl_user')
    var tbl_product = sequelize.import('./tbl_product')

    tbl_user.hasMany(tbl_feedback,{foreignKey: "user_id"})
    tbl_product.hasMany(tbl_feedback,{foreignKey: "product_id"})

    tbl_feedback.sync()
    return tbl_feedback
}