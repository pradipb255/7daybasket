module.exports = function(sequelize,DataType){
    var tbl_wallet_transaction = sequelize.define("tbl_wallet_transaction",{
        user_id:{
            type:DataType.INTEGER
        },
        DebitCredit:{
            type:DataType.INTEGER
        },
        amount:{
            type:DataType.INTEGER
        },
        type:{
            type:DataType.INTEGER
        },
        description:{
            type:DataType.STRING
        }
    },{
        tableName:"tbl_wallet_transaction"
    })

    var tbl_user = sequelize.import('./tbl_user')
    tbl_user.hasMany(tbl_wallet_transaction,{
        foreignKey:"user_id"
    })

    tbl_wallet_transaction.sync()
    return tbl_wallet_transaction
}