module.exports = function(sequelize,DataType){
    var tbl_order = sequelize.define("tbl_order",{
        user_id:{
            type:DataType.INTEGER,
        },
        totalAmount:{
            type:DataType.INTEGER
        },
        offer_id:{
            type:DataType.INTEGER
        },
        discount_amount:{
            type:DataType.INTEGER,
            defaultValue:0
        },
        wallet_amount:{
            type:DataType.INTEGER
        },
        paymenttype:{
            type:DataType.INTEGER
        },
        address:{
            type:DataType.STRING
        },
        status:{
            type:DataType.STRING,
            defaultValue:"placed"
        }
    },{
        tableName:"tbl_order"
    })

    var tbl_user = sequelize.import('./tbl_user')
    var tbl_offer = sequelize.import('./tbl_offer')

    tbl_order.belongsTo(tbl_user,{
        foreignKey:"user_id"
    })
    tbl_offer.hasMany(tbl_order,{
        foreignKey:"offer_id"
    })

    tbl_order.sync()
    return tbl_order
}