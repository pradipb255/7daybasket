var multer = require('multer')
var express = require('express')
var router = express.Router()
var crypto = require('crypto')
var path = require('path')
var sequelize = require('./connection/connection')
var tbl_user = sequelize.import('./models/tbl_user')
var async = require('async')
const isImage = require('is-image')

const storage = multer.diskStorage({
    destination: 'public/images/user/',
    filename: function (req, file, callback) {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            if (err) return callback(err);
          
            callback(null, raw.toString('hex') + path.extname(file.originalname));
          });
    }
  });

  var upload = multer({storage:storage})

router.post('/', upload.single('profile'), (req, res ,next) => {
    data = req.body;
    if (!req.file) {
      return res.send({
        isSuccess: 0,
        Message:"no file selected"
      });
  
    } else {
      const host = req.hostname;
      console.log(req.file.filename)
        const filePath = req.file.path;
        const filename = req.file.filename
        if(isImage(filePath)){
            //edited
        sequelize.authenticate()
        .then(()=>{

            async.waterfall([
                function(callback){
                    if(data.name != ""){
                        if(data.email!="" && data.email.match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$")){
                            if(/^[1-9]{1}[0-9]{9}$/.test(data.mobile)==true){
                                if(data.address!=""){
                                    if(/^\d{6}$/.test(data.pincode)==true){
                                        callback(null)
                                    }
                                    else{
                                        callback("pincode not valid")
                                    }
                                }
                                else{
                                    callback("address not valid")
                                }
                            }
                            else{
                                callback("mobile not valid")
                            }
                        }
                        else{
                            callback("email not valid")
                        }
                    }
                    else{
                        callback("name can not empty")
                    }
                },
                function(callback){
                    tbl_user.find({
                        where:{
                            id:data.id,
                            Isdeleted:0
                        }
                    })
                    .then(user=>{
                        if(user!=null){
                            callback(null,user)
                        }
                        else if(user==null){
                            callback("User not found")
                        }
                    },(err)=>{
                        callback(err)
                    })
                },
                function(user,callback){
                    tbl_user.update({
                        profile:filename,
                        name:data.name,
                        email:data.email,
                        mobile:data.mobile,
                        address:data.address,
                        pincode:data.pincode,
                        updatedBy:user.id
                    },{
                        where:{
                            id:data.id,
                            Isdeleted:0
                        }
                    })
                    .then(user=>{
                        if(user==1){
                            res.send({
                                isSuccess:1,
                                Message:"Record successfully saved"
                            })
                        }
                        else{
                            callback("somthing wrong") 
                        }
                    },(err)=>{
                        callback(err["original"]["sqlMessage"])
                    })
                }
            ],
            function(err,result){
                res.send({
                    isSuccess:0,
                    Message:err,
                    Response:[]
                })
            })
        },(err)=>{
            res.send({
                isSuccess:0,
                Message:err,
                Response:[]
            })
        })
      //edited
        }
        else{
            res.send({
                isSuccess: 0,
                Message:"Selected file is not image"
              });
        }
    }
  });

module.exports = router;