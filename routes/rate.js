var express = require('express')
var router = express.Router()
var sequelize = require('./connection/connection')
var tbl_rate = sequelize.import('./models/tbl_rate')
var async = require('async')

function check_user_id(user_id){
    if(user_id=="" || user_id==null || user_id==0){
        return false
    }else{
        return true
    }
}
check_product_id=(product_id)=>{
    if(product_id=="" || product_id==null || product_id==0){
        return false
    }else{
        return true
    }
}
check_message=(message)=>{
    if(message=="" || message==null){
        return false
    }else{
        return true
    }
}
check_rate=(rate)=>{
    if(rate=="" || rate==null){
        return false
    }else{
        return true
    }
}

router.post('/',function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(check_user_id(data.user_id)){
                    if(check_product_id(data.product_id)){
                        if(check_message){
                            if(check_rate(data.rate)){
                                callback(null)
                            }else{
                                callback("Rate can not be null")
                            }
                        }else{
                            callback("message can not be null")
                        }
                    }else{
                        callback("product_id can not be null or zero")
                    }
                }else{
                    callback("user_id can not be null or zero")
                }
            },
            function(callback){
                tbl_rate.create({
                    user_id:data.user_id,
                    product_id:data.product_id,
                    message:data.message,
                    rate:data.rate
                })
                .then((rate)=>{
                    if(rate!=null){
                        res.send({
                            isSuccess:1,
                            Message:"Thank you for rating"
                        })
                    }else{
                        callback("failed")
                    }
                },(err)=>{
                    callback("you already give rate")
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },err=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

module.exports = router