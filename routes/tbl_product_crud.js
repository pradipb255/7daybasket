var multer = require('multer')
var crypto = require('crypto')
var path = require('path')
var express = require('express')
var router = express.Router()
var sequelize = require('./connection/connection')
var tbl_product = sequelize.import('./models/tbl_product')
var tbl_price = sequelize.import('./models/tbl_price')
var async = require('async')
const isImage = require('is-image')
var Op = sequelize.Op
var Fn = sequelize.fn
var defaultPath = "/images/product/"

const storage = multer.diskStorage({
    destination: 'public/images/product/',
    filename: function (req, file, callback) {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            if (err) return callback(err);
          
            callback(null, raw.toString('hex') + path.extname(file.originalname));
          });
    }
  });
  var upload = multer({storage:storage})

router.post('/add', upload.single('product'), (req, res ,next) => {
    data = req.body

    if (!req.file) {
        return res.send({
          isSuccess: 0,
          Message:"no file selected"
        });
    }else{
        const host = req.hostname;
        const filePath = req.file.path;
        const filename = req.file.filename;
        if(isImage(filePath)){

            sequelize.authenticate()
            .then(()=>{
                async.waterfall([
                    function(callback){
                        if(data.category_id!="" && data.category_id!=null){
                            if(data.product_name!="" && data.product_name!=null){
                                callback(null)
                            }
                            else{
                                callback("product_name can not be null")
                            }
                        }
                        else{
                            callback("category_id can not be null")
                        }
                    },
                    function(callback){
                        tbl_product.create({
                            category_id:data.category_id,
                            product_name:data.product_name,
                            product_image:filename
                        })
                        .then((product)=>{
                            if(product!=null){
                                // res.send({
                                //     isSuccess:1,
                                //     Message:"Product added successfully",
                                //     Response:product
                                // })
                                callback(null,product)
                            }
                            else{
                                callback("Somthing wend wrong")
                            }
                        },(err)=>{
                            callback(err["original"]["sqlMessage"])
                        })
                    },
                    function(product,callback){
                        const host = req.hostname;
                        product.product_image =  defaultPath + product.product_image
                        res.send({
                            isSuccess:1,
                            Message:"Product added successfully",
                            Response:product
                        })
                    }
                ],
                function(err,result){
                    res.send({
                        isSuccess:0,
                        Message:err,
                        Response:[]
                    })
                })
            },(err)=>{
                res.send({
                    isSuccess:0,
                    Message:err,
                    Response:[]
                })
            })

        }
        else{
            res.send({
                isSuccess: 0,
                Message:"Selected file is not image"
            });
        }
    }

  })

  router.post('/getbycategory',(req,res,next)=>{
      sequelize.authenticate()
      .then(()=>{
          data=req.body

          async.waterfall([
              function(callback){
                  if(data.category_id == "" || data.category_id == null){
                      callback("catgory_id can not be null")
                  }
                  else{
                      callback(null)
                  }
              },
              function(callback){
                tbl_product.findAll({
                    raw:true,
                    attributes:["tbl_product.*","tbl_price.price"],
                    include:[{
                        model:tbl_price,
                        attributes:[],
                        where:{
                            flag:1
                        }
                    }],
                    where:{
                        category_id:data.category_id
                    }
                })
                //sequelize.query("SELECT tbl_product.*,tbl_price.price from tbl_product, tbl_price WHERE tbl_product.id = tbl_price.product_id AND tbl_price.id IN (SELECT max(id) FROM tbl_price GROUP by tbl_price.product_id) AND tbl_product.category_id="+data.category_id)
                .then((product)=>{
                    if(product.length!=0){
                        // res.send({
                        //     isSuccess:1,
                        //     Message:"Product found",
                        //     Response:product[0]
                        // })
                       callback(null,product)
                      }
                      else{
                          callback("product not found")
                      }
                  },(err)=>{
                      callback(err)
                  })
                },
                function(product,callback){
                    for(i=0;i<product.length;i++){
                        product[i].product_image = defaultPath + product[i].product_image;
                    }
                    res.send({
                        isSuccess:1,
                        Message:"Product found",
                        Response:product
                    })
                }
            ],
            function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
                Response:[]
            })
        })
      },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
            Response:[]
        })
      })
  })

router.post('/get/',(req,res,next)=>{
    sequelize.authenticate()
    .then(()=>{
        data=req.body

        async.waterfall([
            function(callback){
                if(data.id=="" || data.id==null){
                    callback("id can not be empty")
                }
                else{
                    callback(null)
                }
            },
            function(callback){
                tbl_product.find({
                    include:[{
                        model:tbl_price,
                        attributes:["price"],
                        where:{
                            product_id:data.id
                        }
                    }],
                    order:[[tbl_price,"createdAt","DESC"]]
                },{
                    where:{
                        id:data.id
                    }
                })
                .then((product)=>{
                    if(product!=null){
                        callback(null,product)
                    }
                    else{
                        callback("Product not found")
                    }
                },(err)=>{
                    callback(err)
                })
            },
            function(product,callback){
                product.product_image = defaultPath + product.product_image
                res.send({
                    isSuccess:1,
                    Message:"Product found",
                    Response:product
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
                Response:[]
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
            Response:[]
        })
    })
})

router.get('/get/',(req,res,next)=>{
    sequelize.authenticate()
    .then(()=>{

        async.waterfall([
            function(callback){
                tbl_product.findAll()
                .then((product)=>{
                    if(product!=""){
                        callback(null,product)
                    }
                    else{
                        callback("Product not found")
                    }
                },(err)=>{
                    callback(err["original"]["sqlMessage"])
                })
            },
            function(product,callback){
                for(i=0;i<product.length;i++){
                    product[i].product_image = defaultPath + product[i].product_image
                }
                res.send({
                    isSuccess:1,
                    Message:"Product found",
                    Response:product
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
                Response:[]
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
            Response:[]
        })
    })
})

router.post('/update/', upload.single('product'),(req,res,next)=>{
    data = req.body

        if (!req.file) {
            return res.send({
              isSuccess: 0,
              Message:"no file selected"
            });
        }else{
            const host = req.hostname;
            const filePath = req.file.path;
            const filename = req.file.filename;
            if(isImage(filePath)){
                sequelize.authenticate()
                .then(()=>{
                    async.waterfall([
                        function(callback){
                            if(data.id!=null && data.id!=""){
                                if(data.category_id!="" && data.category_id!=null){
                                    if(data.product_name!="" && data.product_name!=null){
                                        callback(null)
                                    }
                                    else{
                                        callback("product_name can not be null")
                                    }
                                }
                                else{
                                    callback("category_id can not be null")
                                }
                            }
                            else{
                                callback("Id can not be null")
                            }
                        },
                        function(callback){
                            tbl_product.update({
                                category_id:data.category_id,
                                product_name:data.product_name,
                                product_image:filename
                            },{
                                where:{
                                    id:data.id
                                }
                            })
                            .then((product)=>{
                                if(product==1){
                                    res.send({
                                        isSuccess:1,
                                        Message:"Product updated successfully"
                                    })
                                }
                                else{
                                    callback("Product not found")
                                }
                            },(err)=>{
                                callback(err["original"]["sqlMessage"])
                            })
                        }
                    ],
                    function(err,result){
                        res.send({
                            isSuccess:0,
                            Message:err,
                            Response:[]
                        })
                    })
                },(err)=>{
                    res.send({
                        isSuccess:0,
                        Message:err,
                        Response:[]
                    })
                })
            }
            else{
                res.send({
                    isSuccess: 0,
                    Message:"Selected file is not image"
                });
            }
        }
})

router.delete('/delete/',(req,res,next)=>{
    sequelize.authenticate()
    .then(()=>{
        data = req.body
        async.waterfall([
            function(callback){
                if(data.id=="" || data.id==null){
                    callback("Id can not be empty")
                }
                else{
                    callback(null)
                }
            },
            function(callback){
                tbl_product.destroy({
                    where:{
                        id:data.id
                    }
                })
                .then((product)=>{
                    if(product==1){
                        res.send({
                            isSuccess:1,
                            Message:"Product deleted successfully"
                        })
                    }
                    else{
                        callback("Product not found")
                    }
                },(err)=>{
                    callback(err["original"]["sqlMessage"])
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
                Response:[]
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
            Response:[]
        })
    })
})

module.exports = router