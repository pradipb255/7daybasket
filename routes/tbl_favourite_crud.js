var express = require('express')
var router = express.Router()
var sequelize = require('./connection/connection')
var tbl_favourite = sequelize.import('./models/tbl_favourite')
var tbl_product = sequelize.import('./models/tbl_product')
var tbl_price = sequelize.import('./models/tbl_price')
var async = require('async')
var defaultPath = "/images/product/"

router.post('/add',function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.user_id=="" || data.user_id==null){
                    callback("user_id can not be null")
                }else{
                    if(data.product_id=="" || data.product_id==null){
                        callback("product_id can not be null")
                    }else{
                        callback(null)
                    }
                }
            },
            function(callback){
                tbl_favourite.create({
                    user_id:data.user_id,
                    product_id:data.product_id,
                    status:1
                })
                .then((record)=>{
                    if(record!=null){
                        res.send({
                            isSuccess:1,
                            Message:"Product added to favourite list",
                        })
                    }else{
                        callback("request failed")
                    }
                },(err)=>{
                    callback("product already in favourite list or not found")
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

router.post('add',function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.user_id=="" || data.user_id==null){
                    callback("user_id can not be null")
                }else{
                    if(data.product_id=="" || data.product_id==null){
                        callback("product_id can not be null")
                    }else{
                        callback(null)
                    }
                }
            },
            function(callback){
                tbl_favourite.findAll({
                    where:{
                        user_id:data.user_id,
                        product_id:data.product_id
                    }
                })
                .then((record)=>{
                    if(record[0]==null){
                        callback(null)
                    }else{
                        if(record[0].status==true){
                            callback("Product already in favourite list")
                        }else{
                            tbl_favourite.update({
                                status:1
                            },{
                                where:{
                                    user_id:data.user_id,
                                    product_id:data.product_id
                                }
                            })
                            .then((record)=>{
                                console.log(record)
                                if(record==1){
                                    res.send({
                                        isSuccess:1,
                                        Message:"Product added to favourite list"
                                    })
                                }else{
                                    callback("request failed")
                                }
                            })
                        }
                    }
                })
            },
            function(callback){
                tbl_favourite.create({
                    user_id:data.user_id,
                    product_id:data.product_id,
                    status:1
                })
                .then((record)=>{
                    if(record!=null){
                        res.send({
                            isSuccess:1,
                            Message:"Product added to favourite list",
                        })
                    }else{
                        callback("request failed")
                    }
                },(err)=>{
                    callback(err)
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

router.post('/get',function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.user_id=="" || data.user_id==null){
                    callback("user_id can not be null")
                }else{
                    callback(null)
                }
            },
            function(callback){
                tbl_favourite.findAll({
                    raw:true,
                    attributes:["product_id","tbl_product.product_name","tbl_product.product_image","tbl_product->tbl_price.price"],
                    include:[{
                        model:tbl_product,
                        attributes:[],
                        include:[{
                            model:tbl_price,
                            attributes:[],
                            where:{
                                flag:1
                            }
                        }]
                    }],
                    where:{
                        user_id:data.user_id
                    }
                })
                .then((record)=>{
                    if(record[0]!=null){
                        callback(null,record)
                    }else{
                        callback("favourite list is empty")
                    }
                },(err)=>{
                    callback(err)
                })
            },
            function(record,callback){
                for(i=0;i<record.length;i++){
                    record[i].product_image = defaultPath + record[i].product_image;
                }
                res.send({
                    isSuccess:1,
                    Message:"favourite list found",
                    Response:record
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

router.post("/remove",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.user_id=="" || data.user_id==null){
                    callback("user_id can not be null")
                }else{
                    if(data.product_id=="" || data.product_id==null){
                        callback("product_id can not be null")
                    }else{
                        callback(null)
                    }
                }
            },
            function(callback){
                tbl_favourite.destroy({
                    where:{
                        user_id:data.user_id,
                        product_id:data.product_id
                    }
                })
                .then((product)=>{
                    if(product==1){
                        res.send({
                            isSuccess:1,
                            Message:"product removed from favourite list"
                        })
                    }else{
                        callback("product not found in favourite list")
                    }
                    res.send(product)
                },(err)=>{
                    callback(err)
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

router.post('remove',function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.user_id=="" || data.user_id==null){
                    callback("user_id can not be null")
                }else{
                    if(data.product_id=="" || data.product_id==null){
                        callback("product_id can not be null")
                    }else{
                        callback(null)
                    }
                }
            },function(callback){
                tbl_favourite.find({
                    where:{
                        user_id:data.user_id,
                        product_id:data.product_id
                    }
                })
                .then((record)=>{
                    if(record!=null){
                        if(record.status==false){
                            callback("Product already removed from favourite list")
                        }else{
                            callback(null)
                        }
                    }
                    else{
                        callback("product not found in favourite list")
                    }
                },(err)=>{
                    callback(err)
                })
            },
            function(callback){
                tbl_favourite.update({
                    status:0
                },{
                    where:{
                        user_id:data.user_id,
                        product_id:data.product_id
                    }
                })
                .then((record)=>{
                    if(record==1){
                        res.send({
                            isSuccess:1,
                            Message:"product removed from favourite list"
                        })
                    }
                },(err)=>{
                    callback(err)
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

module.exports = router