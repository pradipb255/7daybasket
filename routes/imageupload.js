var multer = require('multer')
var express = require('express');
var router = express.Router();
var crypto = require('crypto')
var path = require('path');

const storage = multer.diskStorage({
    destination: 'public/images/',
    filename: function (req, file, callback) {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            if (err) return callback(err);
          
            callback(null, raw.toString('hex') + path.extname(file.originalname));
          });
    }
  });

  var upload = multer({storage:storage})

router.post('/', upload.single('profile'), (req, res) => {
    console.log(req.body.json)
    if (!req.file) {
      console.log("No file received");
      return res.send({
        isSuccess: 0
      });
  
    } else {
      console.log('file received');
      const host = req.hostname;
        const filePath = req.file.path;
      return res.send({
        isSuccess: 1,
        filePath:filePath
      })
    }
  });

  module.exports = router;