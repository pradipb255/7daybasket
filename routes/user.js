var express = require('express');
var router = express.Router();
var sequelize = require('./connection/connection')
var tbl_user = sequelize.import('./models/tbl_user')
var async = require('async')
var defaultPath = "/images/user/"

function check_user_id(user_id){
    if(user_id=="" || user_id==null || user_id==0){
        return false
    }else{
        return true
    }
}

router.post('/get',function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(check_user_id(data.user_id)){
                    callback(null)
                }else{
                    callback("user_id can not be null")
                }
            },
            function(callback){
                tbl_user.findById(data.user_id)
                .then((user)=>{
                    if(user!=null){
                        user.profile = defaultPath + user.profile
                        res.send({
                            isSuccess:1,
                            Message:"User found",
                            Response:user
                        })
                    }else{
                        callback("User not found")
                    }
                },(err)=>{
                    callback(err)
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err
        })
    })
})

router.post('/update', function(req, res, next) {
    sequelize.authenticate()
    .then(()=>{
        data=req.body

        async.waterfall([
            function(callback){
                if(data.name != ""){
                    if(data.email!="" && data.email.match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$")){
                        if(/^[1-9]{1}[0-9]{9}$/.test(data.mobile)==true){
                            if(data.address!=""){
                                if(/^\d{6}$/.test(data.pincode)==true){
                                    callback(null)
                                }
                                else{
                                    callback("pincode not valid")
                                }
                            }
                            else{
                                callback("address not valid")
                            }
                        }
                        else{
                            callback("mobile not valid")
                        }
                    }
                    else{
                        callback("email not valid")
                    }
                }
                else{
                    callback("name can not empty")
                }
            },
            function(callback){
                tbl_user.find({
                    where:{
                        id:data.id,
                        Isdeleted:0
                    }
                })
                .then(user=>{
                    if(user!=null){
                        callback(null,user)
                    }
                    else if(user==null){
                        callback("User not found")
                    }
                },(err)=>{
                    callback(err["original"]["sqlMessage"])
                })
            },
            function(user,callback){
                tbl_user.update({
                    name:data.name,
                    email:data.email,
                    mobile:data.mobile,
                    address:data.address,
                    pincode:data.pincode,
                    updatedBy:user.id
                },{
                    where:{
                        id:data.id,
                        Isdeleted:0
                    }
                })
                .then(user=>{
                    if(user==1){
                        res.send({
                            isSuccess:1,
                            Message:"Record successfully saved"
                        })
                    }
                    else{
                        callback("somthing wrong") 
                    }
                },(err)=>{
                    callback(err["original"]["sqlMessage"])
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err
        })
    })
});

module.exports = router;