var express = require('express');
var router = express.Router();
var sequelize = require('./connection/connection')
var async = require('async')
var tbl_order = sequelize.import('./models/tbl_order')
var tbl_order_item = sequelize.import('./models/tbl_order_item')
var tbl_product = sequelize.import('./models/tbl_product')

/* GET AboutUs page. */
router.post('/', function(req, res, next) {
  data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.order_id==null || data.order_id=="" || data.order_id==0){
                    callback("order_id can not be null or zero")
                }else{
                    callback(null)
                }
            },
            function(callback){
                tbl_order.find({
                    include:[{
                        attributes:["product_id","quantity","price"],
                        model:tbl_order_item,
                        include:[{
                            model:tbl_product,
                            attributes:["product_name","product_image"]
                        }]
                    }],
                    where:{
                        id:data.order_id
                    }
                })
                .then((order)=>{
                    if(order==null){
                        callback("No order found")
                    }else{
                        // res.send({
                        //     isSuccess:1,
                        //     Message:"Order found",
                        //     Response:order
                        // })
                        res.render('aboutus', {
                             title: 'About us' ,
                             name:'pradip',
                             order:order
                        });
                        //callback(null,order)
                    }
                },(err)=>{
                    callback(err)
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
});

module.exports = router;