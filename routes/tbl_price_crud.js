var express = require('express')
var router = express.Router()
var sequelize = require('./connection/connection')
var tbl_price = sequelize.import('./models/tbl_price')
var async = require('async')

router.post('/add',function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.product_id == null || data.product_id==""){
                    callback("product_id can not be null")
                }
                else{
                    if(data.price == null || data.price == ""){
                        callback("price can not be null")
                    }
                    else{
                        callback(null)
                    }
                }
            },
            function(callback){
                tbl_price.update({
                    flag:0
                },{
                    where:{
                        flag:1,
                        product_id:data.product_id
                    }
                }).then(()=>{
                    return tbl_price.create({
                        product_id:data.product_id,
                        price:data.price
                    })
                })
                .then((product_price)=>{
                    if(product_price!=null){
                        res.send({
                            isSuccess:1,
                            Message:"Price updated successfully",
                            Response:product_price
                        })
                    }
                    else{
                        callback("Somthing went wrong")
                    }
                },(err)=>{
                    callback(err["original"]["sqlMessage"])
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
                Response:[]
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
            Response:[]
        })
    })
})

module.exports = router