var multer = require('multer')
var crypto = require('crypto')
var path = require('path')
const isImage = require('is-image')
var express = require('express')
var router = express.Router()
var sequelize = require('./connection/connection')
var tbl_product_category = sequelize.import('./models/tbl_product_category')
var async = require('async')
var defaultPath = "/images/category/"

const storage = multer.diskStorage({
    destination: 'public/images/category/',
    filename: function (req, file, callback) {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            if (err) return callback(err);
          
            callback(null, raw.toString('hex') + path.extname(file.originalname));
          });
    }
});
var upload = multer({storage:storage})

router.post('/add', upload.single('category'),(req,res,next)=>{
    data = req.body
    if (!req.file) {
        return res.send({
          isSuccess: 0,
          Message:"no file selected"
        });
    }else{
        const host = req.hostname;
        const filePath = req.file.path;
        const filename = req.file.filename;
        if(isImage(filePath)){
            sequelize.authenticate()
            .then(()=>{
                async.waterfall([
                    function(callback){
                        if(data.category_name=="" || data.category_name==null){
                            callback("category_name can not empty")
                        }
                        else{
                            callback(null)
                        }
                    },
                    function(callback){
                        tbl_product_category.create({
                            category_name:data.category_name,
                            category_image:filename
                        })
                        .then((category)=>{
                            if(category!=""){
                                callback(null,category)
                            }
                            else{
                                callback("Somthing went wrong")
                            }
                        },(err)=>{
                            callback(err["original"]["sqlMessage"])
                        })
                    },
                    function(category,callback){
                        category.category_image = defaultPath + category.category_image
                        res.send({
                            isSuccess:1,
                            Message:"category successfully added",
                            Response:category
                        })
                    }
                ],
                function(err,result){
                    res.send({
                        isSuccess:0,
                        Message:err,
                        Response:[]
                    })
                })
        
            },(err)=>{
                res.send({
                    isSuccess:0,
                    Message:err,
                    Response:[]
                })
            })
        }
        else{
            res.send({
                isSuccess: 0,
                Message:"Selected file is not image"
            });
        }
    }
})

router.post('/get/',(req,res,next)=>{
    sequelize.authenticate()
    .then(()=>{
        data=req.body

        async.waterfall([
            function(callback){
                if(data.id=="" || data.id==null){
                    callback("Id can not be null")
                }
                else{
                    callback(null)
                }
            },
            function(callback){
                tbl_product_category.find({
                    where:{
                        id:data.id
                    }
                })
                .then((category)=>{
                    if(category!=null){
                        callback(null,category)
                    }
                    else{
                        callback("category not found")
                    }
                },(err)=>{
                    callback(err["original"]["sqlMessage"])
                })
            },
            function(category,callback){
                category.category_image = defaultPath + category.category_image
                res.send({
                    isSuccess:1,
                    Message:"category found",
                    Response:category
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
                Response:[]
            })
        })

    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
            Response:[]
        }) 
    })
})

router.get('/get/',(req,res,next)=>{
    sequelize.authenticate()
    .then(()=>{
        data = req.body

        async.waterfall([
            function(callback){
                tbl_product_category.findAll()
                .then((category)=>{
                    if(category!=""){
                        callback(null,category)
                    }
                    else if(category==""){
                        callback("category not found")
                    }
                },(err)=>{
                    callback(err["original"]["sqlMessage"])
                })
            },
            function(category,callback){
                for(i=0;i<category.length;i++){
                    category[i].category_image = defaultPath + category[i].category_image
                }
                res.send({
                    isSuccess:1,
                    Message:"category found",
                    Response:category
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
                Response:[]
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
            Response:[]
        })
    })
})

router.post('/update/', upload.single('category'),(req,res,next)=>{
    data = req.body
    console.log(data.id)
    if (!req.file) {
        return res.send({
          isSuccess: 0,
          Message:"no file selected"
        });
    }else{
        const host = req.hostname;
        const filePath = req.file.path;
        const filename = req.file.filename;
        if(isImage(filePath)){
            sequelize.authenticate()
            .then(()=>{

                async.waterfall([
                    function(callback){
                        if(data.id!="" && data.id!=null){
                            if(data.category_name!="" && data.category_name!=null){
                                callback(null)
                            }
                            else{
                                callback("category_name can not be null")
                            }
                        }
                        else{
                            callback("Id can not be null")
                        }
                    },
                    function(callback){
                        tbl_product_category.update({
                            category_name:data.category_name,
                            category_image:filename
                        },{
                            where:{
                                id:data.id
                            }
                        })
                        .then((category)=>{
                            if(category==1){
                                res.send({
                                    isSuccess:1,
                                    Message:"category updated successfully"
                                })
                            }
                            else{
                                callback("category not found")
                            }
                        },(err)=>{
                            callback(err["original"]["sqlMessage"])
                        })
                    }
                ],
                function(err,result){
                    res.send({
                        isSuccess:0,
                        Message:err,
                        Response:[]
                    })
                })
            },(err)=>{
                res.send({
                    isSuccess:0,
                    Message:err,
                    Response:[]
                })
            })
        }else{
            res.send({
                isSuccess: 0,
                Message:"Selected file is not image"
            });
        }

    }
})

router.delete('/delete/',(req,res,next)=>{
    sequelize.authenticate()
    .then(()=>{
        data = req.body

        async.waterfall([
            function(callback){
                if(data.id!="" && data.id!=null){
                    callback(null)
                }
                else{
                    callback("Id can not be empty")
                }
            },
            function(callback){
                tbl_product_category.destroy({
                    where:{
                        id:data.id
                    }
                })
                .then((category)=>{
                    if(category==1){
                        res.send({
                            isSuccess:1,
                            Message:"category deleted successfully"
                        })
                    }
                    else{
                        callback("category not found")
                    }
                },(err)=>{
                    callback(err["original"]["sqlMessage"])
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
                Response:[]
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
            Response:[]
        })
    })
})

module.exports = router;