var express = require('express')
var router = express.Router()
var sequelize = require('./connection/connection')
var async = require('async')
var tbl_wallet_transaction = sequelize.import('./models/tbl_wallet_transaction')
var Op = sequelize.Op

router.post("/get",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.user_id=="" || data.user_id==null || data.user_id==0){
                    callback("user_id can not be null")
                }else{
                    callback(null)
                }
            },
            function(callback){
                tbl_wallet_transaction.findAll({
                    where:{
                        user_id:data.user_id
                    }
                })
                .then((transaction)=>{
                    if(transaction.length!=0){
                        res.send({
                            isSuccess:1,
                            Message:"transaction found",
                            Response:transaction
                        })
                    }else{
                        callback("No transaction found for user")
                    }
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

module.exports = router