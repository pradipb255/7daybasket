var express = require('express');
var router = express.Router();
var sequelize = require('./connection/connection')
var user_code = sequelize.import('./models/user_code')
var async = require('async')



router.post('/', function(req, res, next) {

    sequelize.authenticate()
    .then(()=>{
        data=req.body

        async.waterfall([
            function(callback) {
                if(/^[1-9]{1}[0-9]{9}$/.test(data.mobile)==false){
                    callback("mobile number note valid")
                }
                else{
                    callback()
                }
            },
            function(callback) {
                user_code.create({
                    mobile:data.mobile,
                    code:Math.floor(1000 + Math.random() * 9000)
                })
                .then(()=>{
                    res.send({
                        isSuccess:1,
                        Message:"code is sent to mobile number",
                        Response:[]
                    })
                },(err)=>{
                    callback(err["original"]["sqlMessage"])
                })
            }
        ],
        function(err, results) {
            res.send({
                isSuccess:0,
                Message:err,
                Response:[]
            })
        });
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
            Response:[]
        })
     })
});

module.exports = router;