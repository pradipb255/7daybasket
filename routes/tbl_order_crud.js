var express = require("express")
var router = express.Router()
var sequelize = require('./connection/connection')
var async = require('async')
var tbl_order = sequelize.import('./models/tbl_order')
var tbl_order_item = sequelize.import('./models/tbl_order_item')
var tbl_price = sequelize.import('./models/tbl_price')
var tbl_product = sequelize.import('./models/tbl_product')
var tbl_cart = sequelize.import('./models/tbl_cart')
var tbl_user = sequelize.import('./models/tbl_user')
var tbl_offer = sequelize.import('./models/tbl_offer')
var tbl_wallet_transaction = sequelize.import('./models/tbl_wallet_transaction')
var Op = sequelize.Op

router.post("/place",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.user_id=="" || data.user_id==null || data.user_id==0){
                    callback("user_id can not be null or zero")
                }else{
                    callback(null)
                }
            },
            function(callback){
                tbl_cart.findAll({
                    where:{
                        user_id:data.user_id,
                        Ischeckout:0
                    }
                })
                .then((cart)=>{
                    if(cart.length!=0){
                        for(i=0;i<cart.length;i++){
                            cart[i]=cart[i]["dataValues"]
                        }
                        callback(null,cart)
                    }else{
                        callback("cart is empty")
                    }
                },(err)=>{
                    callback(err)
                })
            },
            function(cart,callback){
                cart.totalAmount = 0
                for(i=0;i<cart.length;i++){
                    cart.totalAmount += cart[i].price
                }
                if(data.offer_id != "" && data.offer_id != null && data.offer_id != 0){
                    tbl_offer.findById(data.offer_id)
                    .then(offer=>{
                        if(offer!=null){
                            discount = cart.totalAmount * offer.discount / 100
                            if(discount>offer.maxdiscount){
                                discount = offer.maxdiscount
                            }
                            cart.totalAmount = cart.totalAmount - discount
                            cart.discount_amount = discount
                            callback(null,cart)
                        }else{
                            callback("offer not found")
                        }
                    },err=>{
                        callback(err)
                    })
                }else{
                    callback(null,cart)
                }
            },
            function(cart,callback){
                if(data.wallet_amount!="" && data.wallet_amount!=null && data.wallet_amount!=0){
                    cart.wallet_amount=data.wallet_amount
                    callback(null,cart)
                }else{
                    callback(null,cart)
                }
            },
            function(cart,callback){
                tbl_order.create({
                    user_id:data.user_id,
                    totalAmount:cart.totalAmount,
                    offer_id:data.offer_id,
                    paymenttype:data.paymenttype,
                    address:data.address,
                    discount_amount:cart.discount_amount,
                    wallet_amount:cart.wallet_amount
                })
                .then((order)=>{
                    if(order!=null){
                        if(cart.wallet_amount!=null && cart.wallet_amount!=""){
                            tbl_user.update({
                                wallet:sequelize.literal("wallet -" + data.wallet_amount)
                            },{
                                where:{
                                    id:data.user_id
                                }
                            })
                            .then(()=>{
                                tbl_wallet_transaction.create({
                                    user_id:data.user_id,
                                    DebitCredit:0,
                                    amount:data.wallet_amount,
                                    type:3,
                                    description:"for order No : " + order.id
                                })
                                .then(()=>{
                                    callback(null,order,cart)
                                })
                            })
                        }else{
                            callback(null,order,cart)
                        }
                    }else{
                        callback("Failed to place order")
                    }
                })
            },
            function(order,cart,callback){
                var product_ids=[]
                for(i=0;i<cart.length;i++){
                    product_ids[i]=cart[i].product_id
                    cart[i].order_id=order.id
                }
                tbl_order_item.bulkCreate(cart)
                .then((order)=>{
                    if(order!=null){
                        tbl_cart.destroy({
                            where:{
                                user_id:data.user_id,
                                product_id:{
                                    [Op.in]:product_ids
                                },
                                Ischeckout:0
                            }
                        })
                        .then((cart)=>{
                            res.send({
                                isSuccess:1,
                                Message:"Order placed successfully",
                                Response:{
                                    order_id:order[0].order_id
                                }
                            })
                        },(err)=>{
                            callback(err)
                        })
                    }else{
                        callback("Failed to place order")
                    }
                },(err)=>{
                    callback(err)
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

router.post("/add",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.product.length == 0){
                    callback("No product received")
                }else{
                    if(data.user_id=="" || data.user_id==null || data.user_id==0){
                        callback("user_id can not be null or zero")
                    }
                    for(i=0;i<data.product.length;i++){
                        if(data.product[i].product_id=="" || data.product[i].product_id==null || data.product[i].product_id==0){
                            callback("product_id can not be null or zero")
                        }
                    }
                    for(i=0;i<data.product.length;i++){
                        if(data.product[i].quantity=="" || data.product[i].quantity==null || data.product[i].quantity==0){
                            callback("quantity can not be null or zero")
                        }
                    }
                    callback(null)
                }
            },
            function(callback){
                var product_ids=[],quantitys=[]
                for(i=0;i<data.product.length;i++){
                    product_ids[i]=data.product[i].product_id
                    quantitys[i]=data.product[i].quantity
                }
                tbl_price.findAll({
                    attributes:["price"],
                    where:{
                        product_id:{
                            [Op.in]:product_ids
                        },
                        flag:1
                    }
                })
                .then((price)=>{
                    data.totalAmount = 0
                    for(i=0;i<data.product.length;i++){
                        data.product[i].price = price[i].price * data.product[i].quantity
                        data.totalAmount += data.product[i].price
                    }
                    callback(null,product_ids)
                    //res.send(data)
                })
            },
            function(product_ids,callback){
                tbl_order.create({
                    user_id:data.user_id,
                    totalAmount:data.totalAmount
                })
                .then((order)=>{
                    if(order!=null){
                        // res.send({
                        //     isSuccess:1,
                        //     Message:"Order placed successfully",
                        //     Response:order
                        // })
                        callback(null,order,product_ids)
                    }else{
                        callback("Failed to place order")
                    }
                })
            },
            function(order,product_ids,callback){
                for(i=0;i<data.product.length;i++){
                    //Object.assign(data.product[i],{"order_id":order.id})
                    data.product[i].order_id=order.id
                }
                tbl_order_item.bulkCreate(
                    data.product
                )
                .then((order)=>{
                    if(order!=null){
                        tbl_cart.destroy({
                            where:{
                                user_id:data.user_id,
                                product_id:{
                                    [Op.in]:product_ids
                                },
                                Ischeckout:0
                            }
                        })
                        .then((cart)=>{
                            res.send({
                                isSuccess:1,
                                Message:"Order placed successfully",
                                Response:{
                                    order_id:order[0].order_id
                                }
                            })
                        },(err)=>{
                            callback(err)
                        })
                    }else{
                        callback("Failed to place order")
                    }
                },(err)=>{
                    callback(err)
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

router.post("/get",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.user_id==null || data.user_id=="" || data.user_id==0){
                    callback("user_id can not be null or zero")
                }else{
                    callback(null)
                }
            },
            function(callback){
                tbl_order.findAll({
                    include:[{
                        attributes:["product_id","quantity","price"],
                        model:tbl_order_item,
                        include:[{
                            model:tbl_product,
                            attributes:["product_name","product_image"]
                        }]
                    }],
                    where:{
                        user_id:data.user_id
                    }
                })
                .then((order)=>{
                    if(order==null){
                        callback("No order found")
                    }else{
                        res.send({
                            isSuccess:1,
                            Message:"Order found",
                            Response:order
                        })
                        //callback(null,order)
                    }
                },(err)=>{
                    callback(err)
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

router.post("/update",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.order_id==null || data.order_id=="" || data.order_id==0){
                    callback("order_id can not be null or zero")
                }else{
                    if(data.status=="" || data.status==null){
                        callback("status can not be null")
                    }else{
                        callback(null)
                    }
                }
            },
            function(callback){
                tbl_order.update({
                    status:data.status
                },{
                    where:{
                        id:data.order_id,
                        user_id:data.user_id
                    }
                })
                .then((order)=>{
                    if(order[0]==1){
                        res.send({
                            isSuccess:1,
                            Message:"Status updated ",
                        })
                    }else{
                        callback("Status update failed")
                    }
                },(err)=>{
                    callback(err)
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

module.exports = router