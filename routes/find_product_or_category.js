var express = require('express')
var router = express.Router()
var sequelize = require('./connection/connection')
var tbl_product = sequelize.import('./models/tbl_product')
var tbl_product_category = sequelize.import('./models/tbl_product_category')
var async = require('async')
var Op = sequelize.Op

router.post('/',function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.keyword=="" || data.keyword==null){
                    callback("No match found for null value")
                }else{
                    callback(null)
                }
            },
            function(callback){
                Promise.all([
                    tbl_product.findAll({
                        attributes:[[sequelize.literal("product_name"),"name"],[sequelize.literal("id"),"product_id"]],
                        where:{
                            product_name:{
                                [Op.like]:"%"+data.keyword+"%"
                            }
                        }
                    }),
                    tbl_product_category.findAll({
                        attributes:[[sequelize.literal("category_name"),"name"],[sequelize.literal("id"),"category_id"]],
                        where:{
                            category_name:{
                                [Op.like]:"%"+data.keyword+"%"
                            }
                        }
                    })
                ])
                //sequelize.query("SELECT product_name,'product' from tbl_product WHERE product_name LIKE '%"+data.keyword+"%' UNION SELECT category_name,'category' FROM tbl_product_category WHERE category_name LIKE '%"+data.keyword+"%'")
                .then((record)=>{
                    if(record.length!=0){
                        res.send({
                            isSuccess:1,
                            Message:"match found",
                            Response:record
                        })
                    }else{
                        callback("No match found")
                    }
                })
                .catch(err=>{
                    callback(err)
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

module.exports = router