var express = require('express');
var router = express.Router();
var sequelize = require('./connection/connection')
var user_code = sequelize.import('./models/user_code')
var tbl_user = sequelize.import('./models/tbl_user')
var tbl_wallet_transaction = sequelize.import('./models/tbl_wallet_transaction')
var async = require('async')
var Op = sequelize.Op
var couponCode = require('coupon-code')

router.post('/', function(req, res, next) {
    sequelize.authenticate()
    .then(()=>{
        data=req.body

        async.waterfall([
            function(callback){
                user_code.find({
                    where:{
                        mobile:data.mobile,
                        code:data.code,
                    }
                })
                .then((user=>{
                    if(user!=null){
                        callback(null,user)
                    }
                    else if(user==null){
                        callback("Wrong code entered")
                    }
                }),(err)=>{
                    callback(err["original"]["sqlMessage"])
                })
            },
            function(user,callback){
                var validtime = user.createdAt
                validtime.setMinutes ( validtime.getMinutes() + 2 );
                if(validtime >= new Date()){
                    tbl_user.find({
                        where:{
                            mobile:data.mobile,
                            Isdeleted:0,
                        }
                    })
                    .then((user)=>{
                        if(user!=null){
                            if(user.email!=null){
                                res.send({
                                    isSuccess:1,
                                    Message:"User found",
                                    Response:[user]
                                })
                            }
                            if(user.email==null){
                                res.send({
                                    isSuccess:1,
                                    Message:"User not Registered",
                                    Response:[{
                                        userid:user.id
                                    }]
                                })
                            }
                        }
                        else if(user==null){
                            tbl_user.create({
                                mobile:data.mobile,
                                refferalCode:couponCode.generate({ partLen: 5, parts: 1})
                            })
                            .then((user)=>{
                                res.send({
                                    isSuccess:1,
                                    Message:"User not Registered",
                                    Response:[{
                                        userid:user.id
                                    }]
                                })
                                if(data.refferalCode!="" && data.refferalCode!=""){
                                    callback(null,user)   
                                }
                            },(err)=>{
                                callback(err["original"]["sqlMessage"])
                            })
                        }
                    },(err)=>{
                        callback(err["original"]["sqlMessage"])
                    })
                }
                else{
                    callback("Code is expired")
                }
            },
            function(target_user,callback){
                tbl_user.find({
                    where:{
                        refferalCode:data.refferalCode
                    }
                })
                .then((user)=>{
                    if(user!=null){
                        Promise.all[
                            tbl_user.update({
                                wallet:sequelize.literal("wallet + 50")
                            },{
                                where:{
                                    [Op.or]:[{
                                        id:user.id
                                    },{
                                        id:target_user.id
                                    }]
                                }
                            }),
                            tbl_wallet_transaction.bulkCreate([
                                {
                                    user_id:target_user.id,
                                    DebitCredit:1,
                                    amount:50,
                                    type:1,
                                    description:"Refferal apply of " + user.mobile
                                },{
                                    user_id:user.id,
                                    DebitCredit:1,
                                    amount:50,
                                    type:1,
                                    description:"Refferal sharing by " + target_user.mobile
                                }
                            ])
                        ]
                    }
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
                Response:[]
            })
        })
    },err=>{
        res.send({
            isSuccess:0,
            Message:err,
            Response:[]
        })
    })
});

module.exports = router;