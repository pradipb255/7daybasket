var express = require('express')
var router = express.Router()
var sequelize = require('./connection/connection')
var tbl_cart = sequelize.import('./models/tbl_cart')
var tbl_product = sequelize.import('./models/tbl_product')
var tbl_price = sequelize.import('./models/tbl_price')
var async = require('async')
var defaultPath = "/images/product/"

router.post("/add",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.user_id=="" || data.user_id==null || data.user_id==0){
                    callback("user_id can not be null or zero")
                }else{
                    if(data.product_id=="" || data.product_id==null || data.user_id==0){
                        callback("product_id can not be null or zero")
                    }else{
                        callback(null)
                    }
                }
            },
            function(callback){
                tbl_price.find({
                    attributes:["price"],
                    where:{
                        product_id:data.product_id,
                        flag:1
                    }
                })
                .then((price)=>{
                    if(price!=null){
                        data.price=price.price
                        callback(null)
                    }else{
                        callback("product not found")
                    }
                },(err)=>{
                    callback(err)
                })
            },
            function(callback){
                tbl_cart.create({
                    user_id:data.user_id,
                    product_id:data.product_id,
                    price:data.price
                })
                .then((record)=>{
                    if(record!=null){
                        res.send({
                            isSuccess:1,
                            Message:"added to cart",
                            Responsr:record
                        })
                    }else{
                        callback("failed to add cart")
                    }
                },(err)=>{
                    callback("product already in cart or not exist")
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

router.post("/updatequantity",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.user_id=="" || data.user_id==null || data.user_id==0){
                    callback("user_id can not be null or zero")
                }else{
                    if(data.product_id=="" || data.product_id==null || data.user_id==0){
                        callback("product_id can not be null or zero")
                    }else{
                        if(data.quantity=="" || data.quantity==null || data.quantity==0){
                            callback("quantity can not ne null or zero")
                        }else{
                            callback(null)
                        }
                    }
                }
            },
            function(callback){
                tbl_price.find({
                    attributes:["price"],
                    where:{
                        product_id:data.product_id,
                        flag:1
                    }
                })
                .then((price)=>{
                    if(price!=null){
                        data.price=price.price * data.quantity
                        callback(null)
                    }else{
                        callback("product not found")
                    }
                },(err)=>{
                    callback(err)
                })
            },
            function(callback){
                tbl_cart.update({
                    quantity:data.quantity,
                    price:data.price
                },{
                    where:{
                        user_id:data.user_id,
                        product_id:data.product_id,
                        Ischeckout:0
                    }
                })
                .then((record)=>{
                    if(record[0]==1){
                        res.send({
                            isSuccess:1,
                            Message:"quantity updated"
                        })
                    }else{
                        callback("Product not fuond in cart")
                    }
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

router.post("/remove",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.user_id=="" || data.user_id==null || data.user_id==0){
                    callback("user_id can not be null or zero")
                }else{
                    if(data.product_id=="" || data.product_id==null || data.user_id==0){
                        callback("product_id can not be null or zero")
                    }else{
                        callback(null)
                    }
                }
            },
            function(callback){
                tbl_cart.destroy({
                    where:{
                        user_id:data.user_id,
                        product_id:data.product_id,
                        Ischeckout:0
                    }
                })
                .then((record)=>{
                    if(record==1){
                        res.send({
                            isSuccess:1,
                            Message:"product removed from cart"
                        })
                    }else{
                        callback("product not found in cart")
                    }
                    res.send(record)
                },(err)=>{
                    callback(err)
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

router.post("/get",function(req,res,next){
    data = req.body
    sequelize.authenticate()
    .then(()=>{
        async.waterfall([
            function(callback){
                if(data.user_id=="" || data.user_id==null || data.user_id==0){
                    callback("user_id can not be null or zero")
                }else{
                    callback(null)
                }
            },
            function(callback){
                tbl_cart.findAll({
                    raw:true,
                    attributes:["quantity","price","tbl_product.*"],
                    include:[{
                        model:tbl_product,
                        attributes:[]
                    }],
                    where:{
                        user_id:data.user_id,
                        Ischeckout:0
                    }
                })
                .then((record)=>{
                    if(record[0]==null){
                        callback("cart is empty")
                    }else{
                        
                        callback(null,record)
                    }
                },(err)=>{
                    callback(err)
                })
            },
            function(record,callback){
                for(i=0;i<record.length;i++){
                    record[i].product_image = defaultPath + record[i].product_image
                }
                res.send({
                    isSuccess:1,
                    Message:"product found in cart",
                    Response:record
                })
            }
        ],
        function(err,result){
            res.send({
                isSuccess:0,
                Message:err,
            })
        })
    },(err)=>{
        res.send({
            isSuccess:0,
            Message:err,
        })
    })
})

module.exports = router