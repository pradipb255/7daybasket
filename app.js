var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyparser = require('body-parser');
var mustacheExpress = require('mustache-express');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var aboutus = require('./routes/aboutus')
var termandcondition = require('./routes/termandcondition')
var loginRouter = require('./routes/login');
var verifynumber = require('./routes/verifynumber')
var user = require('./routes/user')
var imageupload = require('./routes/imageupload')
var updateprofile = require('./routes/updateprofile')
var product_crud = require('./routes/tbl_product_crud')
var product_category_crud = require('./routes/product_category_crud')
var tbl_price_crud = require('./routes/tbl_price_crud')
var tbl_favourite_crud = require('./routes/tbl_favourite_crud')
var find = require('./routes/find_product_or_category')
var cart = require('./routes/tbl_cart_crud')
var order = require('./routes/tbl_order_crud')
var offer = require('./routes/tbl_offer_crud')
var transaction = require('./routes/tbl_wallet_transaction_crud')
var feedback = require('./routes/feedback')
var rate = require('./routes/rate')

var app = express();

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

app.engine('mustache', mustacheExpress());
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'mustache');

//app.engine('mustache', mustacheExpress());
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true}));
app.use(bodyparser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '/public/')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/aboutus',aboutus)
app.use('/termandcondition',termandcondition)
app.use('/login',loginRouter)
app.use('/verifynumber',verifynumber)
app.use('/user',user)
app.use('/imageupload',imageupload)
app.use('/updateprofile',updateprofile)
app.use('/product',product_crud)
app.use('/product_category',product_category_crud)
app.use('/price',tbl_price_crud)
app.use('/favourite',tbl_favourite_crud)
app.use('/find',find)
app.use('/cart',cart)
app.use('/order',order)
app.use('/offer',offer)
app.use('/transaction',transaction)
app.use('/feedback',feedback)
app.use('/rate',rate)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
